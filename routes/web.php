<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "PostController@index");

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// User Panel
Route::group([
    'middleware'    => ['auth'],
    'prefix'        => 'panel',
    'namespace'     => 'Panel',
    'as'            => 'panel.'
], function() {

    Route::resource('posts', 'PostController');

    Route::resource('comments', 'CommentController');
});

Route::get('posts', 'PostController@index')->name('posts.index');
Route::get('posts/top/{count}', 'PostController@getTops')->name('posts.get_tops');
Route::get('posts/{slug}', 'PostController@show')->name('posts.show');

