<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class PostController extends Controller
{
    const POST_PER_PAGE = 5;
    const TOP_COUNT = 10;

    public function index()
    {
        $posts = Post::with(['author'])->paginate(self::POST_PER_PAGE);

        return view("front.posts.index", compact('posts'));
    }

    public function show($slug)
    {
        $id = explode('-', $slug)[0];
        $post = Post::detail()->findOrFail($id);

        return view('front.posts.show', compact('post'));
    }

    public function getTops($count = self::TOP_COUNT)
    {
//        $posts = Cache::rememberForever('posts-10', function() use($count)
//        {
//            return Post::detail()->take($count)->get();
//        });


        $posts = Post::detail()->take($count)->get();

        $top = true;

        return view("front.posts.index", compact('posts', 'top'));
    }


}
