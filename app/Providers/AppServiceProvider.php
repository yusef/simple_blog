<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind("App\Services\Analytics\AnalyticsInterface", "App\Services\Analytics\MatomoAnalytics");
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind("App\Services\AnalyticsInterface", App\Services\MatomoAnalytics::class);
    }
}
