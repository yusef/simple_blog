<?php

namespace App\Models;

use App\Models\Scopes\OrderScope;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'body', 'user_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    // protected $visible = [];

    protected $appends = ['slug'];


    protected static function boot() {
        parent::boot();
        static::addGlobalScope(new OrderScope('id', 'desc'));
    }


    /**
     * --------------------------------------------------------------------------
     * SCOPE
     * --------------------------------------------------------------------------
     */
    public function scopeDetail($q)
    {
        return $q->with(['comments.author', 'author']);
    }

    /**
     * --------------------------------------------------------------------------
     * RELATIONS
     * --------------------------------------------------------------------------
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     *  --------------------------------------------------------------------------
     *  ACCESORS
     *  --------------------------------------------------------------------------
     */
    public function getSlugAttribute()
    {
        return $this->id . "-" . str_replace(" ", "-", $this->title);
    }

    public function getDateAttribute()
    {
        return dateFormat($this->created_at, 'Y-m-d');
    }

}
