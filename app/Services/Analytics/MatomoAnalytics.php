<?php
namespace App\Services\Analytics;

use RobBrazier\Piwik\Facades\Piwik;

class MatomoAnalytics implements AnalyticsInterface {

    public function addUser($args) {
        
        $res = Piwik::getUsersManager()->addUser(
            'test3',
            '123456',
            'ali1@gmail.com'
        );
        return json_encode($res);
    }

    public function addWebsite($args) {
        return "addWebsite";
    }
}