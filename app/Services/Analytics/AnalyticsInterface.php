<?php

namespace App\Services\Analytics;

interface AnalyticsInterface {
    public function addUser($args);
    public function addWebsite($args);
}