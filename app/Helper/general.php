<?php

function displayAlert()
{
    if (\Illuminate\Support\Facades\Session::has('message'))
    {
        return sprintf('<div class="alert alert-info">%s</div>', \Illuminate\Support\Facades\Session::get('message'));
    }

    if (\Illuminate\Support\Facades\Session::has('error'))
    {
        return sprintf('<div class="alert alert-danger">%s</div>', \Illuminate\Support\Facades\Session::get('error'));
    }

    return '';
}

function displayErrors($errors)
{
    $result = "";
    if(isset($errors))
        foreach ($errors->all() as $error)
            $result = $result . sprintf('<div class="alert alert-danger">%s</div>', $error);
    return $result;
}

function dateFormat($timesamp, $format)
{
    return \Carbon\Carbon::parse($timesamp)->format($format);
}

const TOP_POST_COUNT = 10;