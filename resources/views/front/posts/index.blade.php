@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @foreach($posts as $post)
                    <div class="card mt-2">
                        <div class="card-header"><a href="{{route('posts.show', $post->slug)}}">{{ $post->title }}</a></div>
                        <div class="card-body">
                            {{ $post->body }}
                            <div>
                                <ul>
                                    <li><small>author: {{$post->author->name}}</small></li>
                                </ul>
                            </div>
                        </div>

                        @if(isset($top) And $top == true)
                            @foreach($post->comments as $comment)
                                <div class="col-md-10">
                                    <div class="card mt-2">

                                        <div class="card-header">author: {{$comment->author->name}}   <small>{{$comment->date}}</small></div>
                                        <div class="card-body">
                                            {{ $comment->body }}
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                        @endif


                    </div>
                @endforeach
                    @if ($posts instanceof \Illuminate\Pagination\LengthAwarePaginator)
                        {{ $posts->onEachSide(5)->links() }}
                    @endif
            </div>
        </div>
    </div>
@endsection
