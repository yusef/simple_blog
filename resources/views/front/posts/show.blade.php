@extends('layouts.app')

@section('content')
    <!--Modal: Login / Register Form-->
    <div class="modal fade" id="modalLRForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog cascading-modal" role="document">
            <!--Content-->
            <div class="modal-content">

                <!--Modal cascading tabs-->
                <div class="modal-c-tabs">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs md-tabs tabs-2 light-blue darken-3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#panel7" role="tab"><i class="fas fa-user mr-1"></i>
                                Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#panel8" role="tab"><i class="fas fa-user-plus mr-1"></i>
                                Register</a>
                        </li>
                    </ul>

                    <!-- Tab panels -->
                    <div class="tab-content">
                        <!--Panel 7-->
                        <div class="tab-pane fade in show active" id="panel7" role="tabpanel">

                            <form action="{{ route('login') }}" method="POST">
                                @csrf
                                <div class="modal-body mb-1">
                                    <div class="md-form form-sm mb-5">
                                        <i class="fas fa-envelope prefix"></i>
                                        <input type="email" name="email" id="modalLRInput10" class="form-control form-control-sm validate">
                                        <label data-error="wrong" data-success="right" for="modalLRInput10">Your email</label>
                                    </div>

                                    <div class="md-form form-sm mb-4">
                                        <i class="fas fa-lock prefix"></i>
                                        <input type="password" name="password" id="modalLRInput11" class="form-control form-control-sm validate">
                                        <label data-error="wrong" data-success="right" for="modalLRInput11">Your password</label>
                                    </div>
                                    <div class="text-center mt-2">
                                        <button type="submit" class="btn btn-info">Log in <i class="fas fa-sign-in ml-1"></i></button>
                                    </div>

                                </div>
                            </form>

                            <!--Footer-->
                            <div class="modal-footer">
                                <div class="options text-center text-md-right mt-1">
                                    <p>Not a member? <a href="#" class="blue-text">Sign Up</a></p>
                                </div>
                                <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
                            </div>

                        </div>
                        <!--/.Panel 7-->

                        <!--Panel 8-->
                        <div class="tab-pane fade" id="panel8" role="tabpanel">

                            <!--Body-->
                            <form action="{{ route('register') }}" method="POST">
                                @csrf
                                <div class="modal-body">
                                    <div class="md-form form-sm mb-5">
                                        <i class="fas fa-envelope prefix"></i>
                                        <input type="text" name="name" id="modalLRInput12" class="form-control form-control-sm validate">
                                        <label data-error="wrong" data-success="right" for="modalLRInput12">name</label>
                                    </div>
                                    <div class="md-form form-sm mb-5">
                                        <i class="fas fa-envelope prefix"></i>
                                        <input type="email" name="email" id="modalLRInput12" class="form-control form-control-sm validate">
                                        <label data-error="wrong" data-success="right" for="modalLRInput12">Your email</label>
                                    </div>

                                    <div class="md-form form-sm mb-5">
                                        <i class="fas fa-lock prefix"></i>
                                        <input type="password" name="password" id="modalLRInput13" class="form-control form-control-sm validate">
                                        <label data-error="wrong" data-success="right" for="modalLRInput13">Your password</label>
                                    </div>

                                    <div class="md-form form-sm mb-4">
                                        <i class="fas fa-lock prefix"></i>
                                        <input type="password" name="password_confirmation" id="modalLRInput14" class="form-control form-control-sm validate">
                                        <label data-error="wrong" data-success="right" for="modalLRInput14">Repeat password</label>
                                    </div>

                                    <div class="text-center form-sm mt-2">
                                        <button class="btn btn-info">Sign up <i class="fas fa-sign-in ml-1"></i></button>
                                    </div>

                                </div>
                            </form>
                            <!--Footer-->
                            <div class="modal-footer">
                                <div class="options text-right">
                                    <p class="pt-1">Already have an account? <a href="#" class="blue-text">Log In</a></p>
                                </div>
                                <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        <!--/.Panel 8-->
                    </div>

                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!--Modal: Login / Register Form-->
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-8">
                {!! displayAlert() !!}
                {!! displayErrors($errors) !!}
                <div class="card mt-2">
                    <div class="card-header">{{ $post->title }}</div>
                    <div class="card-body">
                        {{ $post->body }}
                        <div>
                            <ul>
                                <li><small>author: {{$post->author->name}}</small></li>
                            </ul>
                        </div>
                    </div>

                </div>
                <h2 class="mt-3">Comments</h2>
                @foreach($post->comments as $comment)

                    <div class="card mt-2">

                        <div class="card-header">author: {{$comment->author->name}}   <small>{{$comment->date}}</small></div>
                        <div class="card-body">
                            {{ $comment->body }}
                        </div>
                    </div>
                @endforeach

                <div class="card mt-2">
                    <div class="card-header">{{ __('Add Comment') }}</div>
                    <div class="card-body">
                        <form id="add_comment" method="POST" action="{{ route('panel.comments.store') }}">
                            @csrf
                            <input type="hidden" name="post_id" value="{{ $post->id }}">
                            <div class="form-group row">
                                <label for="body" class="col-md-4 col-form-label text-md-right">Body</label>

                                <div class="col-md-6">
                                    <textarea class="form-control @error('title') is-invalid @enderror" id="body" name="body">

                                    </textarea>
                                    @error('body')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button id="submit" type="submit" class="btn btn-primary @if(!auth()->check()) disable @endif"  @if(!auth()->check()) data-toggle="modal" data-target="#modalLRForm" @endif >
                                        {{ __('Submit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>

    <script>

        $(document).ready(function() {
            $('#submit').click(function(e){
                if($(this).hasClass('disable')) {
                    e.preventDefault();
                    console.log($(this).data('target'));
                    $('#modalLRForm').trigger('click');
                    return true;
                }
                return true;
            })
        })
    </script>
@endsection
